<?php
    // include 'connection.php';

?>

<div id="rsvp" class="py-5rem bg-light">
    <div class="container py-5">
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2">
                <h4 class="section-title text-center">RSVP</h4>
                <br>
                <p class="text-center">
                    In light of the current health restrictions in our country, our wedding is sadly smaller than intended. We have reserved a limited of seats for you and hope you can join us as we celebrate our wedding day.
                </p>
                <p class="text-center">
                    The favor of a reply is requested on or before December 20, 2021 via Facebook Event: <a href="https://www.facebook.com/events/1107040993366138/" target="_blank">facebook.com/events/1107040993366138</a>
                    <br>    
                        <a href="https://www.facebook.com/events/1107040993366138/" target="_blank" class="btn btn-dark rounded-pill px-5 py-3 mt-5">Respond</a><br>
                        
                </p>
                

            </div>
        </div>
        <!-- <div class="row">
            <div class="col-12 col-lg-10 offset-lg-1 text-center">
                <br>

                <form action="connection.php" method="post">
                    <h6>Invitation code:</h6>
                    <input class="invitation-code" type="text" name="invite_code" required>
                    <br>
                    <button type="submit" class="btn btn-outline-dark mt-3" name="rsvp">
                        Submit
                    </button>
                </form>
            </div>
        </div> -->
    </div>
</div>

<?php


?>

