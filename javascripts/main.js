function autoPlayYouTubeModal() {
  var trigger = $("body").find('[data-bs-toggle="modal"]');
  trigger.click(function () {
    var theModal = $(this).attr("data-bs-target"),
      videoSRC = $(this).attr("data-theVideo"),
      modalTitle = $(this).attr("data-title"),
      videoSRCauto = videoSRC + "&autoplay=1";

    $(theModal + " .modal-title").text(modalTitle);
    $(theModal + " iframe").attr("src", videoSRCauto);
    $(theModal + " button.btn-close").click(function () {
      $(theModal + " iframe").attr("src", "");
    });
  });
}

$(document).ready(function () {
  autoPlayYouTubeModal();

  $("#gallery").owlCarousel({
    stagePadding: 80,
    loop: true,
    margin: 10,
    nav: false,
    dots: true,
    autoplay: true,
    autoplayHoverPause: true,
    responsive: {
      0: {
        items: 1,
      },
      576: {
        items: 2,
      },
      768: {
        items: 3,
      },
    },
  });

  $(".message-slider").owlCarousel({
    autoplay: false,
    loop: true,
    margin: 10,
    nav: false,
    items: 1,
    autoplayHoverPause: true
  });
});
