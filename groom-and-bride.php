<div id="the-groom" class="py-5rem ">
	<div class="container py-5">
		<div class="row">
			<div class="col-12 col-md-6 order-md-2">
				<div class="text-center">
					<img draggable="false"src="images/the-groom-800x800.jpg"  class="the-groom-image img-fluid rounded-2 mb-5 mb-md-0" alt="">
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div class="d-table h-100">
					<div class="d-table-cell align-middle">
						<h4 class=" section-title text-left">The Groom</h4>																					
						<p>Dhan is a reserved man. He keeps his personal life private and low-key. He practices minimalism and focuses more on quality than quantity. He’s very good at handling his own emotion; he doesn’t get mad or irritate easily; he always thinks outside the box and resolves problem readily. He value loyalty and commitment. He like things to be well organized and has a great deal of attention to detail. He’s an introvert; he finds his peace and energy alone but  has an easygoing personality with others and a great sense of humor. He is a walking encyclopedia and never runs out of trivia.</p>
						<p>The list may go on and on, but for me, Dhan is a gift from God. A gift that I’m excited to unwrap and discover how beautiful my life is because of his presence.</p>
					</div>
				</div>					
			</div>			
		</div>
	</div>
</div>

<div id="the-bride" class="py-5rem bg-light">
	<div class="container py-5">
		<div class="row">
			<div class="col-12 col-md-6">
				<div class="text-center">
					<img draggable="false"src="images/the-bride-800x800.jpg"  class="the-bride-image img-fluid rounded-2 mb-5 mb-md-0" alt="">
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div class="d-table h-100">
					<div class="d-table-cell align-middle">
						<h4 class="section-title text-left">The Bride</h4>
						<p>Michelle is a compassionate person. She cares more about the greater good of others. She has a service-oriented personality, sometimes it's hard for her to say “NO”. She’s willing to do her duties even if will entail sacrifice because Act of Service is her love language. She values peace, calm, and harmony anything that can make her soul rest. She has this warm-hearted and welcoming nature. She loves to inspire, bless and touch people's lives. She is independent, responsible, practical, and knows how to handle her finances. She’s fun of trying new things, challenging herself, and discovering where’s she’s good at. She’s very considerate, forgiving, and untroubled.</p>
						<p>Michelle is a woman of faith, my undeniable and lifetime blessing.</p>
					</div>
				</div>
			</div>			
		</div>
	</div>
</div>