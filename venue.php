<div id="the-wedding" class="py-5rem bg-light">
	<div class="container">
        <div class="row mb-5">
            <div class="col-12">
                <h4 class="section-title text-center">The Wedding</h4>
            </div>
        </div>
        <div class="row d-none">
            <div class="col-12">
                <h4 class="mb-3 text-center">
                    <strong>Primary Sponsors</strong>
                </h4>
            </div>
            <div class="col-12 col-sm-6 text-end">
                Mr. Angelo Lirio<br>
                Mr. Lolito Canares<br>
                Mr. Jun Janoras
            </div>
            <div class="col-12 col-sm-6">
                Mrs. Alma Reisgo<br>
                Mrs. Mary Grace Atilon<br>                
                Mrs. Engelyn Morano
            </div>
        </div>
		<div class="row">            
			<div class="col-12 col-md-6 pb-5 pb-md-0 text-center border-md-end">
                <h4 class="mb-3">
                    <strong>Ceremony</strong>
                </h4>
                <p>
                    January 21, 2022 
                    <br class="d-block d-lg-none">
                    <span class="d-none d-lg-inline-block">&nbsp;|&ensp;</span> 
                    9:00 AM - 11:00 AM
                </p>
                <a href="#" data-bs-target="#video-modal" class="venue-image d-block mb-4 mt-4 position-relative w-75 mx-auto" data-title="Caleruega Transfiguration Chapel" data-theVideo="https://www.youtube.com/embed/x2ELXPDYTlQ?show_text=false"  data-bs-toggle="modal">
                    <i class="fa-5x fa-play-circle far text-white-50" style=""></i>
                    <img draggable="false"class="w-100" src="images/calaruega-transfiguration-chapel.jpg" alt="Caleruega Transfiguration Chapel">
                </a>
                <h6 class="mb-1">Caleruega Transfiguration Chapel</h6>                
                <p>Caylaway, Batulao, Nasugbu, Batangas</p>                
                <a rel="nofollow" href="https://www.google.com/maps/place/Caleruega+Chapel+of+Transfiguration/@14.0726588,120.8350103,120m/data=!3m1!1e3!4m5!3m4!1s0x33bd96b9fe598991:0xe1cbcb1f58a6135!8m2!3d14.0724861!4d120.8352282" target="_blank" class="btn btn-outline-dark mt-3">
                    <i class="fas fa-map-marked-alt"></i> Get direction
                </a>		
			</div>
			<div class="col-12 col-md-6 text-center">
                <h4 class="mb-3">
                    <strong>Reception</strong>
                </h4>
                <p>
                    January 21, 2022 
                    <br class="d-block d-lg-none">
                    <span class="d-none d-lg-inline-block">&nbsp;|&ensp;</span>  
                    12:00 PM - 14:00 PM
                </p>

                <a href="" data-bs-target="#video-modal" class="venue-image d-block mb-4 mt-4 position-relative w-75 mx-auto" data-bs-toggle="modal" data-title="LifePlace Retreat and Event Center" data-theVideo="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Flifeplacealfonso%2Fvideos%2F167763973932739%2F&show_text=false">
                    <i class="fa-5x fa-play-circle far text-white-50" style=""></i>
                    <img draggable="false"class="w-100" src="images/lifeplace-pavilion.jpg" alt="LifePlace Pavilion">
                </a>
                
                <h6 class="mb-1">LifePlace Retreat and Event Center</h6>
                
                <p> Patutong Malaki North, Alfonso, Cavite </p>
                
                <a rel="nofollow" href="https://www.google.com/maps/place/Life+Place/@14.1039404,120.8564523,17z/data=!3m1!4b1!4m5!3m4!1s0x33bd9d1a54efe36b:0x9abb1ba582910f80!8m2!3d14.1039352!4d120.858641" target="_blank" class="btn btn-outline-dark mt-3">
                    <i class="fas fa-map-marked-alt"></i> Get direction
                </a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" data-bs-backdrop="static" id="video-modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content bg-transparent border-0">
            <div class="modal-header text-white px-0 border-0">
                <h5 class="modal-title"></h5>
                <button type="button" class="btn-close bg-white m-0" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            
            <div class="ratio ratio-16x9 border bg-dark">
                <iframe src=""allowfullscreen></iframe>
            </div>										
        </div>
    </div>
</div>