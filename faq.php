<div id="questions" class="py-5rem bg-light">
	<div class="container">        
        <div class="row">
            <div class="col-12 col-lg-8 offset-lg-2">
                <h4 class="section-title text-center">
                    Frequently Asked Questions
                </h4>
                <br>
                <div class="accordion accordion-flush" id="faq">            
                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#faq-dresscode"> What should I wear? Is there a dress code? </button>
                        </h2>
                        <div id="faq-dresscode" class="accordion-collapse collapse show" data-bs-parent="#faq"> 
                            <div class="accordion-body">
                                <ul>
                                    <li>
                                        <strong>Primary Sponsor</strong>
                                        <ul>
                                            <li>
                                                <strong>Gentlemen:</strong> Black Suit with matching trousers white inner, neck tie (any color)  and black shoes
                                            </li>
                                            <li>
                                                <strong>Ladies:</strong> Black/Grey Formal Gown, we encourage you to wear comfortable sandals such as flats or bring flats if needed.
                                            </li>
                                        </ul>                              
                                    </li>
                                    <li>
                                        <strong>Secondary Sponsor</strong>
                                        <ul>
                                            <li>
                                                <strong>Gentlemen: </strong> Black suit jacket, matching trousers, white dress shirt, black bow tie and leather shoes. (We will provide the bow tie)
                                            </li>
                                            <li>
                                                <strong>Ladies:</strong> Black and White Floor-length dress , Rubber shoes / Flats
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <strong>Guest</strong>
                                        <ul>
                                           <li>Wear formal that you’re comfortable In, but please indulge us by dressing in color black/gray if you don't mind</li>
                                        </ul>                                         
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-theme"> Does your wedding have a theme? </button>
                        </h2>
                        <div id="faq-theme" class="accordion-collapse collapse" data-bs-parent="#faq">
                                <div class="accordion-body"> Minimalist </div>
                        </div>
                    </div>
                    
                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-time">
                                What time should we arrive?
                            </button>
                        </h2>
                        <div id="faq-time" class="accordion-collapse collapse" data-bs-parent="#faq">
                            <div class="accordion-body">
                                We recommend that you arrive 30 mins before the start of the ceremony, to make sure everyone is on time .
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-parking">
                                Is there parking available near the ceremony and reception venues?
                            </button>
                        </h2>
                        <div id="faq-parking" class="accordion-collapse collapse" data-bs-parent="#faq">
                            <div class="accordion-body">
                                Yes, both ceremony and reception venue has free parking.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-kids">
                                Are kids welcome?
                            </button>
                        </h2>
                        <div id="faq-kids" class="accordion-collapse collapse" data-bs-parent="#faq">
                            <div class="accordion-body">
                                We wish we could have your little ones, but due to necessity rather than choice, it is children of immediate family only.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-bring">
                                Can I bring + 1?
                            </button>
                        </h2>
                        <div id="faq-bring" class="accordion-collapse collapse" data-bs-parent="#faq">
                            <div class="accordion-body">
                                We have a strict guest list to stay on budget. Our wedding is strictly RSVP only. We will only be able to accommodate those listed on your invitation.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-pictures">
                                Can I take pictures during the ceremony/post on social media?
                            </button>
                        </h2>
                        <div id="faq-pictures" class="accordion-collapse collapse" data-bs-parent="#faq">
                            <div class="accordion-body">                                
                                You may take photos, but we encourage not to specially during the walking in the aisles and ceremony. We have professional photographers/Videographers on hand to capture all the special moments and are excited to share their photos with you.
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-contact">
                                Is there someone I can contact if I have any other questions about the wedding?
                            </button>
                        </h2>
                        <div id="faq-contact" class="accordion-collapse collapse" data-bs-parent="#faq">
                            <div class="accordion-body">
                                You may reach the bride or groom via SMS or Facebook Messenger
                            </div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-gift">
                                Gift guide
                            </button>
                        </h2>
                        <div id="faq-gift" class="accordion-collapse collapse" data-bs-parent="#faq">
                            <div class="accordion-body">
                            If your were thinking of giving a gift to help us on our way, a gift of money in a card would really make our day!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>