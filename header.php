<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
	<meta name="author" content="Dhan &amp; Michelle Wedding">
	<meta name="viewport" content="width=device-width, initial-scale=1">
		
	<title>Mr and Mrs Ortiz</title>

	<meta name="description" content="Dhan &amp; Michelle Wedding" />
	<meta name="robots" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
	<link rel="canonical" href="https://mrandmrsortiz.com" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="Dhan &amp; Michelle Wedding | Jan. 21, 2022" />
	<meta property="og:description" content="Dhan &amp; Michelle Wedding | Jan. 21, 2022" />
	<meta property="og:url" content="https://mrandmrsortiz.com" />
	<meta property="og:site_name" content="Dhan &amp; Michelle Wedding | Jan. 21, 2022" />	
	<meta property="og:image" content="images/dhan-che-yellow-01.jpg" />
	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:label1" content="Est. reading time">
	<meta name="twitter:data1" content="0 minutes">

	<!-- ****** faviconit.com favicons ****** -->
	<link rel="shortcut icon" href="images/faviconit/favicon.ico">
	<link rel="icon" sizes="16x16 32x32 64x64" href="images/faviconit/favicon.ico">
	<link rel="icon" type="image/png" sizes="196x196" href="images/faviconit/favicon-192.png">
	<link rel="icon" type="image/png" sizes="160x160" href="images/faviconit/favicon-160.png">
	<link rel="icon" type="image/png" sizes="96x96" href="images/faviconit/favicon-96.png">
	<link rel="icon" type="image/png" sizes="64x64" href="images/faviconit/favicon-64.png">
	<link rel="icon" type="image/png" sizes="32x32" href="images/faviconit/favicon-32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="images/faviconit/favicon-16.png">
	<link rel="apple-touch-icon" href="images/faviconit/favicon-57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/faviconit/favicon-114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/faviconit/favicon-72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/faviconit/favicon-144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="images/faviconit/favicon-60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="images/faviconit/favicon-120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="images/faviconit/favicon-76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="images/faviconit/favicon-152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="images/faviconit/favicon-180.png">
	<meta name="msapplication-TileColor" content="#FFFFFF">
	<meta name="msapplication-TileImage" content="images/faviconit/favicon-144.png">
	<meta name="msapplication-config" content="images/faviconit/browserconfig.xml">
	<!-- ****** faviconit.com favicons ****** -->

	<!-- ASSETS CDN -->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Cookie&family=Open+Sans:wght@400;600&family=Raleway:wght@300;400;600&display=swap&family=Euphoria+Script&display=swap">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" referrerpolicy="no-referrer" />

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-G116CKX1R6"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  		gtag('js', new Date());

  		gtag('config', 'G-G116CKX1R6');
	</script>

	
	<link href="css/main.css" rel="stylesheet">

  </head>
  <body>