<?php 
	include 'header.php';

	if(!isset($_COOKIE['guest'])) {
		header("Location: index.php#rsvp");
		exit();
	}

	$guest = json_decode($_COOKIE['guest'], true);

?>

<div class="inner-page-banner pt-5 text-center">
    <div class="container">
        <div class="banner-content pt-4">
            <h1 class="">Dhan <span class="">&amp;</span> Michelle</h1>
        </div>
    </div>		
</div>

<div class="section-title-outer">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h4 class="section-title text-center pt-5">RSVP</h4>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-12 col-lg-8 offset-lg-2 pb-5">
			<form action="connection.php" method="post">
				<?php 
					echo "<table class='table mb-5'>";
					echo "<thead>";
					echo "<tr>";
					echo "<th scope='col'>#</th>";
					echo "<th scope='col'>Guest</th>";
					echo "<th scope='col'>Response</th>";
					echo "</tr>";
					echo "</thead>";
					echo "<tbody>";
					foreach( $guest as $key => $g) {
						$guest_id = $g['id'];
						
						$rsvp_accepts = "";
						$rsvp_decline = "";
						if(isset($g['response']) && $g['response'] == 1) {
							$rsvp_accepts = 'checked';
						} else if(isset($g['response']) && $g['response'] == 0) {
							$rsvp_decline = "checked";
						}
						
						echo "<tr>";
						echo "<td>" . ++$key . "</td>";
						echo "<td>" . $g['first_name'] . " " . $g['last_name'] . "</td>";
						echo "<td class='col-response'>
								<div class='form-check d-inline-block me-4 mb-0'>
									<input class='form-check-input' name='response[$guest_id]' type='radio' value='1' id='going-$guest_id' $rsvp_accepts required>
									<label class='form-check-label' for='going-$guest_id'>Joyfully accepts</label>
								</div>
								<div class='form-check d-inline-block mb-0'>
									<input class='form-check-input' name='response[$guest_id]' type='radio' value='0' id='not-going-$guest_id' $rsvp_decline >
									<label class='form-check-label' for='not-going-$guest_id'>Regretfully decline</label>
								</div>
							</td>";
						echo "</tr>";
						
					}
					echo "</tbody>";
					echo "<tfoot>";
					echo "<tr>";
					echo "<td colspan='3' class='text-center border-bottom-0'>";
					echo "<input type='submit' name='rsvp-response' value='Submit' class='btn btn-outline-dark py-2 px-4 mt-2'>";
					echo "</td>";
					echo "</tr>";
					echo "</tfoot>";
					echo "</table>";
				?>
			</form>
		</div>
	</div>
</div>

<div class="inner-page-footer d-none">
</div>

<a href='https://www.freepik.com/vectors/background'>Background vector created by rawpixel.com - www.freepik.com</a>
	
<?php
	include 'footer.php'; 
?>