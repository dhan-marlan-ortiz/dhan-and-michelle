<div id="prefooter-banner" class="py-5rem">
    <div class="container text-center">
        <div class="wrapper">
            <h4 class="text-white pt-5">
                <small>The</small> <strong>BEST GIFT</strong> <small>will be your participation</small>
            </h4>
            <img class="separator-1 py-4" src="images/separator-2.png" alt="separator">
            <p class="text-white thanks pb-5">
                Thank you
            </p>
        </div>
    </div>
</div>