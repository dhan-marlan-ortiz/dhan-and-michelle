<div id="middle-banner" class="py-5rem">
    <div class="container text-center">
        <div class="wrapper">
            <h4 class="text-white pt-5">
                <small>We are so excited to </small> <strong>CELEBRATE</strong> <small>with you</small>
            </h4>
            <img class="separator-1 py-4" src="images/separator-2.png" alt="separator">
            <a target="_blank" href="https://calendar.google.com/event?action=TEMPLATE&amp;tmeid=MHJib3NmYTdwcXZvaG0xZWhwaGN1Mmg5dTYgZGhhbi5tYXJsYW4ub3J0aXpAbQ&amp;tmsrc=dhan.marlan.ortiz%40gmail.com" class="btn btn-light rounded-pill px-4 py-3 mt-3 mb-5" title="Save Event in my Calendar">
            Add to your <i class="fab fa-google"></i>oogle Calendar</a>                                
        </div>
    </div>
</div>