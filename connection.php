<?php

function connect() {
    $servername = "localhost";
    $username = "root";
    $password = "";
    $database = "dhan-and-michelle";
    
    $connection = new mysqli($servername, $username, $password, $database);
    
    if ($connection->connect_error) {
      die("Connection failed: " . $connection->connect_error);
    } 

    return $connection;
}

function disconnect() {

    $connect = connect();
    $connect->close();
}

function getGuest($invite_code = null) {

    $connect = connect();

    if(isset($_POST['invite_code'])) {
        $invite_code = $_POST['invite_code'];
    }
    
    $sql = "SELECT * FROM guest WHERE invite_code = '$invite_code'";
    $result = $connect->query($sql);

    disconnect();

    return $result;
}

function updateGuestResponse($response) {
    $connect = connect();

    $guest = json_decode($_COOKIE['guest'], true);
    $invite_code = $guest[0]['invite_code'];
    
    $data = array();    
  
    foreach ($response AS $key => $value) {
        $data[] = "($key, $value)";
    }
    
    $sql = "INSERT INTO guest(id, response) VALUES " . implode(', ', $data) . " ON DUPLICATE KEY UPDATE response = VALUES(response)";
    
    
    if ($connect->query($sql) === TRUE) {
        echo "Record updated successfully";

        $result = getGuest($invite_code);
    
        if($result->num_rows) {
            $guest = array();
                        
            while($row = $result->fetch_assoc()) {
                $guest[] = $row;
            }

            setcookie('guest', json_encode($guest), time() + (86400 * 30), "/" );
            
        }
    
    } else {
        echo "Error updating record: " . $connect->error;
    }
      
    disconnect();
}

if( isset($_POST['rsvp']) ) {
    $result = getGuest();

    if($result->num_rows) {
        // $token = hash('ripemd160', trim($_POST['invite_code']));
        $guest = array();
        
        while($row = $result->fetch_assoc()) {
            $guest[] = $row;
        }
        
        setcookie('guest', json_encode($guest), time() + (86400 * 30), "/" );

        header("Location: guest.php");
    } else {
        header("Location: index.php?error=invalid+invitation+code#rsvp");
    }
} else if(isset($_POST['rsvp-response'])) {
    $response = $_POST['response'];

    $update = updateGuestResponse($response);
    
    header("Location: guest.php");

} else {
    header("Location: index.php?access=forbidden");
}