<div id="celebrate-with-us" class="py-5rem">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h4 class="section-title text-left text-white">Celebrate with us</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-6">			
                <div class="event-info">
                    <dl class="row">
                        <dt class="col-sm-3 text-md-end">Date:</dt>
                        <dd class="col-sm-9">January 21, 2022</dd>

                        <dt class="col-sm-3 text-md-end">Ceremony:</dt>
                        <dd class="col-sm-9">Caleruega Transfiguration Chapel</dd>

                        <dt class="col-sm-3 text-md-end">Reception:</dt>
                        <dd class="col-sm-9">Lifeplace Event and Retreat Center</dd>
                    </dl>											
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="d-table position-relative h-100 w-100">
                    <div class="d-table-cell align-middle">
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a target="_blank" href="https://calendar.google.com/event?action=TEMPLATE&amp;tmeid=MHJib3NmYTdwcXZvaG0xZWhwaGN1Mmg5dTYgZGhhbi5tYXJsYW4ub3J0aXpAbQ&amp;tmsrc=dhan.marlan.ortiz%40gmail.com" class="btn btn-outline-light rounded-pill px-4 py-3" title="Save Event in my Calendar">
                                    Add to <i class="fab fa-google"></i> Calendar
                                </a>                                
                            </li>
                            <li class="list-inline-item">
                                <a class="btn btn-outline-light rounded-pill px-4 py-3" href="#rsvp">Celebrate with us</a>
                            </li>
                        </ul>
                    </div>
                </div>					
            </div>
        </div>
    </div>
</div>