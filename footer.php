<div id="footer">
	<div class="container">
		<div class="row">
			<div class="col-12 py-5">
				<h3 class="text-center">
					dhan <span>&amp;</span> che</span>
				</h3>
				<p class="text-center text-uppercase">
					<small>
						Copyright &copy; 2021. All rights reserved
						<br>
						by <strong>DMORTIZ</strong>
					</small>
				</p>
			</div>
		</div>
	</div>
</div>

<!-- <script src="javascripts/jquery.mousewheel.min.js" ></script> -->
<!-- <script src="javascripts/jquery.simplr.smoothscroll.js" ></script> -->

<?php 
/*
    if(!in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', "::1") )){
      echo '
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/1ee9e7de96.js" crossorigin="anonymous"></script>';
    } else {
      echo '
        <script src="javascripts/jquery-3.5.1.slim.min.js"></script>
        <script src="javascripts/bootstrap.bundle.min.js"></script>
        <script src="javascripts/fontawesome-kit.js" crossorigin="anonymous"></script>';
    }
*/    
?>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/1ee9e7de96.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>  

<script src="javascripts/main.js" ></script>

</body>

</html>