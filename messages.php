<div id="messages" class="py-5rem">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h4 class="section-title text-center">Messages</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="message-slider owl-carousel owl-theme">
                    
                    <!-- Donnessa -->
                    <div class="item">
                        <blockquote class="blockquote">
                            <img src="images/donne-thumb.jpg" alt="">
                            <div class="content-wrapper">
                                <p>
                                    Dearest Che and Dhan,
                                </p>
                                <p>
                                    Congratulation on one of the most special days of your life. I hope you know that as one of your friends, I have been praying and eagerly waiting for your special day to come. Finally! 😍
                                </p>
                                <p>
                                    Your relationship have been a roller coaster ride, but your strong bond as a friend and as a partner to one another made you stay in love and choose to love again and again, and again and always.
                                </p>
                                <p>
                                    I'd like you also to know that through your relationship you were able to help me cope with the hesitation I felt whenever someone is showing their feelings to me. I remember Che was the very reason why I got the courage to be in love again and Dhan advised me on how long I should let someone court me. I don't know if you still remember those things but I'm pretty sure with what I'm saying right now. 😅  I must say magic happened and that guy is my hubby right now. So sometimes or maybe magic really happens if that particular person is really alloted for you by God no matter how short or long you're together if he/she is for you, then so be it. So this is it! Big day is coming and I will always admire your relationship since day one and to the rest of your lives together.
                                </p>           
                                <p>
                                    <strong> &mdash; Donnessa </strong>
                                </p>                     
                            </div>                            
                        </blockquote>                    
                    </div>

                    <!-- Mommie Bham -->
                    <div class="item">
                        <figure>
                            <blockquote class="blockquote">
                                <img src="images/bham-thumb.jpg" alt="">
                                <div class="content-wrapper">
                                    <p>Finally, this is it pansit 👏🏻👏🏻👏🏻 di ko man nasimulan ang DhanChe love story sobrang happy ako dahil sa mga panahon na nasubaybayan q ang fav loveteam ko eh alam ko na sa altar hahantong ang pagmamahalan nyo… Isa aq sa nagkaabang sa napaka espesyal araw na para sainyong dalawa hindi man ako maging parte neto physically alam nyong sa puso ko anjan ang presensya ko always.  Love ko kaw che ang aking bunsoy pinagpipray q ang masayang pagsasama nyo and  ofcourse kelangan paspasan nyo tulos kay need na ng kalaro ni Emson and baby’O ☺️  Congratulations to you DhanChe. May God Bless your Married life 👏🏻👏🏻👏🏻🙏🙏🙏❤️❤️❤️ </p>
                                    <p>Love,</p>
                                    <p>
                                        <strong>&mdash; Mommie Bham</strong>
                                    </p>
                                </div>
                            <blockquote>
                        </figure>                                                        
                    </div>
                    
                    <!-- Marie Anne -->
                    <div class="item">
                        <blockquote class="blockquote">
                            <img src="images/ann-thumb.jpg" alt="">
                            <div class="content-wrapper">
                                <p>Congratulations on your wedding day. We wish all the best of the world to both of you, love each other, accept each other,help each other and most of all,put Christ in the center of your life.</p>
                                <p>Always keep the fire burning and cherish each moment being together. Congratulations Che and Dhan!❤️❤️❤️👏👏👏👏🎊🎊🎊</p>         
                                <p>
                                    <strong> &mdash; Marie Anne </strong>
                                </p>   
                            </div>
                        </blockquote>
                    </div>            
                    
                </div>
            </div>
        </div>
    </div>
</div>