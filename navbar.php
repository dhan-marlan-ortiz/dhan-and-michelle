<?php 
    include 'header.php';
?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Our Story</a>
                </li>               
                <li class="nav-item">
                    <a class="nav-link" href="#">The Groom</a>
                </li>         
                <li class="nav-item">
                    <a class="nav-link" href="#">Celebrate With Us</a>
                </li>  
                <li class="nav-item">
                    <a class="nav-link" href="#">Wedding Venues</a>
                </li>               
                <li class="nav-item">
                    <a class="nav-link" href="#">RSVP</a>
                </li>     
                <li class="nav-item">
                    <a class="nav-link" href="#">FAQ</a>
                </li>     
            </ul>           
        </div>
    </div>
</nav>

<?php 
    include 'footer.php'; 
?>