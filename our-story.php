<div id="our-story" class="py-5rem">
	<div class="container text-center">

		<div class="row">
			<div class="col-12 col-lg-10 offset-lg-1">
				<h4 class="section-title">Our Story</h4>
				<p>It wasn’t love at first sight; there were no heart-fluttering moments, no magic nor sparks of feeling. It was only an ordinary meeting with an exchange of hi and hellos after we got introduced by a common friend in Netopia.</p>
				<p>A few days after we got introduced, I received a message from him, and guess what he’s talking about? Astronomy. Weird, but good thing there’s an Encarta way back then, so I got my answer to his weirdo question. That is all how we started. The Encarta thing, oh, forget about it; it will just reveal how old we are. Hahaha. After a year of exchanging messages and letters, we became an official couple, and the rest is history.</p>
				<p>Our relationship is not as sweet as HONEY. We had a lot of differences, values, and opinions, that sometimes made us think that we're not meant to BEE. Our relationship is more likely a COFFEE. There were times that we agreed on all things, like sugar, sweetness overload, but most of the time we have a lot of disagreement or conflicts, and just like a coffee ground, it's bitter. But through the years we learn to embrace our differences, our love grows, mature, and continue to bear good BEANS, we just need to meet halfway to realize we're the bittersweet perfect blend couple.</p>
			</div>
		</div>										
	
		<div class="row">
			<div class="col">				
				<div id="gallery" class="owl-carousel owl-them mt-5">
					<div class="item"> <img draggable="false"src="images/carousel/sldr-grp-a-1.jpg" class="rounded-2 img-fluid" alt="slider image"> </div>
					<div class="item"> <img draggable="false"src="images/carousel/sldr-grp-a-2.jpg" class="rounded-2 img-fluid" alt="slider image"> </div>
					<div class="item"> <img draggable="false"src="images/carousel/sldr-grp-a-3.jpg" class="rounded-2 img-fluid" alt="slider image"> </div>
					<div class="item"> <img draggable="false"src="images/carousel/sldr-grp-a-4.jpg" class="rounded-2 img-fluid" alt="slider image"> </div>
					<div class="item"> <img draggable="false"src="images/carousel/sldr-grp-a-5.jpg" class="rounded-2 img-fluid" alt="slider image"> </div>					
					<div class="item"> <img draggable="false"src="images/carousel/sldr-grp-a-6.jpg" class="rounded-2 img-fluid" alt="slider image"> </div>					
					<div class="item"> <img draggable="false"src="images/carousel/sldr-grp-a-7.jpg" class="rounded-2 img-fluid" alt="slider image"> </div>					
					<div class="item"> <img draggable="false"src="images/carousel/sldr-grp-a-8.jpg" class="rounded-2 img-fluid" alt="slider image"> </div>					
					<div class="item"> <img draggable="false"src="images/carousel/sldr-grp-a-9.jpg" class="rounded-2 img-fluid" alt="slider image"> </div>					
				</div>
			</div>
		</div>

	</div>
</div>



